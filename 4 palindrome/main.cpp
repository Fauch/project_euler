#include <iostream>

using namespace std;

//declaring functions

bool is_palindrome(int number);

int main()
{

//declaring variables

int max_number = 9009;

for (int a=999; a>99; a--){
    for (int b=999; b>99; b--){
        int prod=a*b;
        if (is_palindrome(prod)==true) {
            if (prod > max_number)
            max_number=prod;
            }
        }
    }
cout << max_number << '\n';
}

bool is_palindrome(int number)
{
//variables
int check = 0;
int digit = 0;
int ans = 0;
//body

check = number; // set check to the original number

    while (number>0) {
    digit = number%10;
    ans = ans*10+digit;
    number/=10;
    }
if (ans==check)
    return true;
else return false;
}

