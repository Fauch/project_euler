#include "../../std_lib_facilities.h"
#include "stdafx.h"

//Function declaration

int fibonacci(int n);

int _tmain(int argc, _TCHAR* argv[])
{
	// Variablen
	int z = 1;
	int i = 2;
	int sum = 0;
	
	while (z < 4000000) {
		z = fibonacci (i);
		if (z <= 4000000) { 
			cout << z << endl;
			++i;
			}
		if (z%2 == 0)
			sum += z;

		}
	cout << "Die Summe der geraden Zahlen ist " << sum << endl;
	cout << "Press ENTER to continue" << endl;
	cin.ignore (numeric_limits < streamsize > ::max(), '\n');
	return 0;
}

int fibonacci (int n)
{
	int a = 1;
	int b = 2;
	int c;

	for (int i = 1; i <= n; ++i) {
		c = a+b;
		a = b;
		b = c;
	}
return c;
}
		
