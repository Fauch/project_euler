#include <iostream>

using namespace std;

int main()
{
    // Variablen
    int input = 0;
    int sum_of_squares = 0;
    int sum = 0;
    int square_of_sum = 0;
    int difference = 0;

    cout << "Please Enter a number for which you want to calculate the sum_square_difference: ";
    cin >> input;

    // sum of the squares
    for (int i = 1; i <= input; i++){
        sum_of_squares += i * i;
    }

    // square of the sum
    for (int i = 1; i <= input; i++) {
        sum +=i;
    }
    square_of_sum = sum*sum;
    difference = square_of_sum - sum_of_squares;
    // Output
    cout << "The sum of the squares is " << sum_of_squares << endl;

    cout << "The square of the sum is " << square_of_sum << endl;
    cout << "The difference between the sum of the Squares and the Square of the Sum is " << difference << "." ;
    return 0;
}
