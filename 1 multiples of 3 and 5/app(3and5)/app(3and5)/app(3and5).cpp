#include "stdafx.h"
#include "../../std_lib_facilities.h"

bool isDivisible (int number, int divisor);

int _tmain(int argc, _TCHAR* argv[])
{
	// Variablen
	int sum3 = 0; //Summe aller dreier
	int sum5 = 0; //Summe aller f�nfer
	int sum35 = 0; //Summe der Zahlen die durch drei UND f�nf teilbar sind
	int sum_all = 0; // Gesamtsumme

	//Multiples of 3
	for (int i = 1; i < 1000; i++) {
		if ( isDivisible(i,3) == true )
			{
			 sum3 = sum3+i;
			 cout << sum3 << endl;
			}
	}

	//Multiples of 5
	for (int i = 1; i < 1000; i++) {
		if ( isDivisible(i,5) == true)
		{
			sum5 = sum5+i;
			cout << sum5 << endl;
		}
	}
	//Multiples of 5 and 3
	for (int i = 1; i < 1000; i++) {
		if ( (isDivisible (i,3)) && (isDivisible (i,5)) )
		{
			sum35 = sum35+i;
		}
	}


	// Add up
	sum_all = sum5+sum3-sum35;
	cout << "Die Gesamtsumme der Multiplizitaeten von 3 und 5 ist " << sum_all << endl;

	
	cout << "Press ENTER to continue" << endl;
	cin.ignore (numeric_limits <streamsize> ::max (), '\n');
	return 0;
}

bool isDivisible (int number, int divisor)
{
	if (number % divisor == 0)
	{ return true; }
	else return false;
	
}