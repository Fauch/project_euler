#include <iostream>
#include <limits>
#include <math.h>

using namespace std;


bool isPrime (signed long long int number);
bool isDivisible (signed long long int number, signed long long int Divisor);



int main()
{
    // Variablen

    signed long long int input = 0;

    // Input

    cout << "Please Enter the number of which you want to find the prime Factors: ";
    cin >> input;


    // Main Loop

    for (long long int i = 1; i <= sqrt(input); i++) {
            if ( isDivisible(input,i) && isPrime(i) )
                cout << i << endl;

    }

    cout << "Press ENTER to continue..." << endl;
    cin.get();
    cin.ignore(numeric_limits < streamsize > ::max(), '\n');
    return 0;
}


bool isDivisible (signed long long int number, signed long long int divisor)
{
  if ( (number % divisor) == 0 )
        return true;
    else return false;
}



bool isPrime (signed long long int number)
{
    for (long long int i = 2; i < number; ++i) {

        if ( isDivisible ( number, i) )
            return false;
        }
return true;
}

