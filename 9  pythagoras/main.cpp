#include <iostream>
#include <math.h>
#include <limits>

using namespace std;


//declare functions
void output (int a, int b, int c);

int main()
{
    // declare variables
    int a=1;
    int b=2;
    int c=3;

    // main loop
    for (int a=1; a<500; a++) {
        for (int b=2; b<500; b++) {
            // int c=1000-a-b;
            // if (a*a+b*b-c*c == 0 && a<b )
            int c=sqrt( (a*a)+(b*b) );
            if (1000-c-a-b == 0 && ((a*a)+(b*b)== c*c) )
                output(a,b,c);
        }
    }

    //end of program
    cout << "Press ENTER to continue...";
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
}


void output (int a, int b, int c)
{
    cout << "a=" << a << '\n';
    cout << "b=" << b << '\n';
    cout << "c=" << c << '\n';
    cout << "a*b*c=" << a*b*c << '\n';

}
